package com.example.openshif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenshifApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenshifApplication.class, args);

        System.out.println("Hollow OpenShift");
    }

}
